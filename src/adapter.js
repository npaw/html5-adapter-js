var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Html5 = youbora.Adapter.extend({
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayhead: function () {
    return this.player.currentTime
  },

  getDuration: function () {
    return this.player.duration
  },

  getIsLive: function () {
    return this.player.duration === Infinity
  },

  getResource: function () {
    return this.player.currentSrc
  },

  getPlayrate: function () {
    return this.player.playbackRate
  },

  getPlayerName: function () {
    return 'html5'
  },

  registerListeners: function () {
    this.isPlaying = false

    this.monitorPlayhead(false, false)

    this.references = {
      play: this.playListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this),
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      error: this.errorListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      ended: this.endedListener.bind(this),
      loadstart: this.loadStartListener.bind(this),
      stalled: this.stalledListener.bind(this),
      waiting: this.bufferingListener.bind(this),
      canplay: this.canPlayListener.bind(this),
      canplaythrough: this.canPlayThroughListener.bind(this),
      loadedmediadata: this.loadedMediaDataListener.bind(this),
      loadeddata: this.loadedDataListener.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    this.isPlaying = false

    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
        delete this.references[key]
      }
    }
  },

  playListener: function (e) {
    if (this.player) {
      this.checkReadyState(this.player.readyState, 'playListener')
    }
    this.fireStart({}, 'playListener')
    this.isPlaying = true
  },

  timeupdateListener: function (e) {
    if ((this.getPlayhead() > 0.1 || (this.plugin && this.plugin.getIsLive())) && !this.player.error) {
      this.fireStart({}, 'timeupdateListener')
      this.fireJoin({}, 'timeupdateListener')
      this.isPlaying = true
    }
  },

  pauseListener: function (e) {
    this.firePause({}, 'pauseListener')
    this.isPlaying = false
  },

  playingListener: function (e) {
    this.fireResume({}, 'playingListener')
    this.fireSeekEnd({}, 'playingListener')
    if (this.flags.isBuffering){
      this.fireBufferEnd({}, 'playingListener')
    }
    this.isPlaying = true
  },

  errorListener: function (e) {
    var msg = null
    var code = null
    try {
      if (e && e.target && e.target.error) {
        code = e.target.error.code
        msg = e.target.error.message
      }
    } catch (err) {
      // nothing
    }
    this.fireError(code, msg, undefined, undefined, 'errorListener')
  },

  seekingListener: function (e) {
    this.fireSeekBegin({}, false, 'seekingListener')
  },

  seekedListener: function (e) {
    this.fireSeekEnd({}, 'seekedListener')
  },

  endedListener: function (e) {
    // And fire stop event
    this.fireStop({}, 'endedListener')
    this.isPlaying = false
  },

  loadStartListener: function (e) {
    if (this.flags.isJoined) {
      // If content is being played and new one is loaded, it stops without 'ended' event
      // If view is active but not showing content yet, skip this call to avoid wrong EBVS
      this.fireStop({}, 'loadStartListener')
    }
  },

  stalledListener: function(e) {
    if(!this.isPlaying && !this.isBuffering){
      this.fireBufferBegin({}, false, 'stalledListener')
    }
  },

  bufferingListener: function(e) {
    this.fireBufferBegin({}, false, 'waitingListener')
  },

  loadedMediaDataListener: function (e) {
    if (this.player) {
      this.checkReadyState(this.player.readyState, 'loadedMediaDataListener')
    }
  },

  canPlayListener: function (e) {
    if (this.player) {
      this.checkReadyState(this.player.readyState, 'canPlayListener')
    }
  },

  canPlayThroughListener: function(e) {
    if (this.player) {
      this.checkReadyState(this.player.readyState, 'canPlayThroughListener')
    }
  },

  loadedDataListener: function(e) {
    if (this.player) {
      this.checkReadyState(this.player.readyState, 'loadedDataListener')
    }
  }
})

module.exports = youbora.adapters.Html5

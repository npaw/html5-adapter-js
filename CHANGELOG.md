## [6.8.10] - 2024-12-17
### Library
- Packaged with `lib  6.8.59`

## [6.8.9] - 2024-05-21
### Fixed
- Sample page video url.
### Library
- Packaged with `lib  6.8.56`

## [6.8.8] - 2022-12-09
### Added
- Condition stalled event listener to avoid trigger buffer on some seek conditions

## [6.8.7] - 2022-11-10
### Added
- Listen more methods to check readyState video property to detect buffer in other platforms
### Library
- Packaged with `lib 6.8.35`

## [6.8.6] - 2022-11-03
### Library
- Packaged with `lib 6.8.34`

## [6.8.5] - 2022-10-21
### Library
- Packaged with `lib 6.8.32`

## [6.8.4] - 2022-06-20
### Added
- Check to end active views when 'loadstart' event, but preventing wrong EBVS
### Library
- Packaged with `lib 6.8.24`

## [6.8.3] - 2022-03-01
### Added
- Check to trigger jointime with timeupdates for live content
### Library
- Packaged with `lib 6.8.12`

## [6.8.2] - 2022-01-27
### Added
- Check to prevent fake views after player erroring and triggering timeupdate after
### Library
- Packaged with `lib 6.8.11`

## [6.8.1] - 2021-08-16
### Added
- `loadstart` event listener to split views

## [6.8.0] - 2021-08-09
### Added
- Listener for waiting event to support `content.isLive.noMonitor` functionality fallback
### Library
- Packaged with `lib 6.8.0`

## [6.7.12] - 2021-07-30
### Fixed
- Added back `seeked` event detection
### Library
- Packaged with `lib 6.7.41`

## [6.7.11] - 2021-05-11
### Library
- Packaged with `lib 6.7.34`

## [6.7.10] - 2021-02-08
### Added
- Playrate detection
### Fixed
- Seek detection for some cases
### Library
- Packaged with `lib 6.7.27`

## [6.7.9] - 2020-12-23
### Added
- IsLive detection comparing the duration with Infinity.
### Library
- Packaged with `lib 6.7.25`

## [6.7.8] - 2020-09-28
### Library
- Packaged with `lib 6.7.17`

## [6.7.7] - 2020-07-21
### Library
- Packaged with `lib 6.7.13`

## [6.7.6] - 2020-07-09
### Library
- Packaged with `lib 6.7.11`

## [6.7.5] - 2020-06-08
### Fixed
- Error not reporting code and messages

## [6.7.4] - 2020-05-21
### Fixed
- Check for error message and code types to report only strings

## [6.7.3] - 2020-05-14
### Library
- Packaged with `lib 6.7.6`

## [6.7.2] - 2020-04-06
### Library
- Packaged with `lib 6.7.2`

## [6.7.1] - 2020-04-02
### Library
- Packaged with `lib 6.7.1`

## [6.7.0] - 2020-03-10
### Added
- Error codes and messages
### Library
- Packaged with `lib 6.7.0`

## [6.5.5] - 2020-01-14
### Library
- Packaged with `lib 6.5.24`

## [6.5.4] - 2019-10-03
### Added
- Unnecesary code removed
### Library
- Packaged with `lib 6.5.16`

## [6.5.3] - 2019-08-30
### Library
- Packaged with `lib 6.5.14`

## [6.5.2] - 2019-07-01
### Fixed
 - Removed unnecesary dependence
### Library
- Packaged with `lib 6.5.5`

## [6.5.1] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.1` (using normal Adapter)

## [6.4.4] - 2019-04-30
### Library
- Packaged with `lib 6.4.25`

## [6.4.3] - 2019-01-30
### Library
- Packaged with `lib 6.4.15`

## [6.4.2] - 2018-08-29
### Library
- Packaged with `lib 6.4.5`

## [6.4.1] - 2018-08-28
### Library
- Packaged with `lib 6.4.4`

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.3.1] - 2018-07-09
### Library
- Packaged with `lib 6.3.2`

## [6.3.0] - 2018-06-19
### Library
- Packaged with `lib 6.3.0` (And using StandardAdapter)
### Fix
- Fixed start and join call order using timeupdate event

## [6.2.1] - 2018-05-02
### Library
- Packaged with `lib 6.2.3`

## [6.2.0] - 2018-04-06
### Library
- Packaged with `lib 6.2.0`
## Add
- Start in timeupdate, to create views when video is not stopped going to background

## [6.1.5] - 2018-03-29
### Library
- Packaged with `lib 6.1.15`

## [6.1.4] - 2018-03-21
### Library
- Packaged with `lib 6.1.14`
### Fix
- Fixed issue with isLive

## [6.1.3] - 2017-12-22
### Library
- Packaged with `lib 6.1.8`

## [6.1.2] - 2017-12-21
### Fix
- Fixed issue with unregisterEvents using reference list

## [6.1.1] - 2017-12-13
### Fix
- Fixed issue with unregisterEvents method names

## [6.1.0] - 2017-11-28
### Library
- Packaged with `lib 6.1.6`

## [6.0.1] - 2017-07-11
### Library
- Packaged with `lib 6.0.3`
### Fix
- Fix problem with manifest and pipelines
### Add
- Add `tech` to `getVersion`.

## [6.0.0] - 2017-05-25
### Library
- Packaged with `lib 6.0.0`
